# -*- coding: utf-8 -*-

from datetime import datetime
import json
import os

from application_only_auth import Client
from flask import Flask
from flask.ext.heroku import Heroku

CONSUMER_KEY = 'fNuBp5mjkppcqmeArzCV2g'
CONSUMER_SECRET = 'MpFmV2rzCkVpVwHj5UQ5RZjKrpbw1JWfXE63myEJmk'

TWITTER_URL = 'https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=%s&count=%s'
CREATED_AT_FORMAT = '%a %b %d %H:%M:%S +0000 %Y'
DATETIME_FORMAT = '%d/%m/%Y %H:%M:%S'

app = Flask(__name__)
heroku = Heroku(app)


@app.route('/')
def index():
    return 'Tweet Mirror'


@app.route('/<screen_name>', methods=['GET'])
@app.route('/<screen_name>/<count>', methods=['GET'])
def get_statuses(screen_name, count=50):
    if not isinstance(screen_name, str):
        screen_name = str(screen_name)

    messages = {'messages': update_statuses(screen_name, count)}
    return json.dumps(messages, ensure_ascii=False)


def update_statuses(screen_name, count=10):
    client = Client(CONSUMER_KEY, CONSUMER_SECRET)
    tweets = client.request(TWITTER_URL % (screen_name, count))

    t = []
    for tweet in tweets:

        created_at = datetime.strptime(tweet['created_at'], CREATED_AT_FORMAT)
        t.append({
            'created_at': created_at.strftime(DATETIME_FORMAT),
            'text': tweet['text'],
            'id': tweet['id']
        })

    return t
