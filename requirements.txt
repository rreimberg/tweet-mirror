Flask==0.10.1
Flask-Heroku-Cacheify==1.3
Flask-Script==0.6.2
flask-heroku==0.1.4
gunicorn==18.0
pylibmc==1.2.3
-e git+https://github.com/rreimberg/twitter-application-only-auth.git#egg=application_only_auth
